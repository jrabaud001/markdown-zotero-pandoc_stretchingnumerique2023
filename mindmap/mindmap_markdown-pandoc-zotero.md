# Pandoc

## Liens officiels


### [Le site officiel](https://pandoc.org)

- [Installing Pandoc](https://pandoc.org/installing.html)
- Les [exemples](https://pandoc.org/demos.html)
- Le très riche *Manuel*
  - html - [Pandoc User’s Guide](https://pandoc.org/MANUAL.html)
  - pdf - [Pandoc User’s Guide](https://pandoc.org/MANUAL.pdf)
- [Pandoc Extras](https://pandoc.org/extras.html)

### [Le démonstrateur en ligne](https://pandoc.org/try/)

### Les sources - [Github](https://github.com/jgm/pandoc)

#### Les Templates - [Github](https://github.com/jgm/pandoc-templates)

### les listes de discussions

- Google Group : [pandoc-discuss](https://groups.google.com/g/pandoc-discuss)
- StackOverflow : [Questions tagged \[pandoc\]](https://stackoverflow.com/questions/tagged/pandoc)


## Cours - Tutoriels - Guides - Ateliers

### *The Programming Historian*

- [Getting Started with Markdown](https://programminghistorian.org/en/lessons/getting-started-with-markdown) - 2015 (màj 2020)
  - fr : [Débuter avec Markdown](https://programminghistorian.org/fr/lecons/debuter-avec-markdown)
  - es : [Introducción a Markdown](https://programminghistorian.org/es/lecciones/introduccion-a-markdown)
  - pt : [Introdução ao Markdown](https://programminghistorian.org/pt/licoes/introducao-ao-markdown)

- [Sustainable Authorship in Plain Text using Pandoc and Markdown](https://programminghistorian.org/en/lessons/sustainable-authorship-in-plain-text-using-pandoc-and-markdown) - 2014 (màj 2022)
  - fr : [Rédaction durable avec Pandoc et Markdown](https://programminghistorian.org/fr/lecons/redaction-durable-avec-pandoc-et-markdown)
  - es : [Escritura sostenible en texto plano usando Pandoc y Markdown](https://programminghistorian.org/es/lecciones/escritura-sostenible-usando-pandoc-y-markdown)
  - pt : [Autoria Sustentável em Texto Simples usando Pandoc e Markdown](https://programminghistorian.org/pt/licoes/autoria-sustentavel-texto-simples-pandoc-markdown)


### La section *Cours* du [blog](https://www.arthurperret.fr) d’*Arthur Perret*

- [Bibliographie](https://www.arthurperret.fr/cours/bibliographie.html)
- [Écriture scientifique au format texte](https://www.arthurperret.fr/cours/ecriture-scientifique-format-texte.html)
- [Expressions régulières](https://www.arthurperret.fr/cours/expressions-regulieres.html)
- [Flux web](https://www.arthurperret.fr/cours/flux-web.html)
- [Format texte](https://www.arthurperret.fr/cours/format-texte.html)
- [Markdown](https://www.arthurperret.fr/cours/markdown.html)
- [Pandoc](https://www.arthurperret.fr/cours/pandoc.html)
- [Publication web](https://www.arthurperret.fr/cours/publication-web.html)
- [Recherche d’information et veille](https://www.arthurperret.fr/cours/recherche-information-veille.html)
- [Sérialisation de données](https://www.arthurperret.fr/cours/serialisation.html)

### Supports du [*Stretching numérique*](https://www.stretchingnumerique.fr) 2022

- [Le markdown comme syntaxe universelle](https://zenodo.org/record/6344346) - Damien Belvèze
- [Supports de formation Obsidian - Markdown](https://zenodo.org/record/6366116) - Jean-Baptiste Monat
- [Support de formation HTML- CSS pour débutants](https://zenodo.org/record/6077289) - Jean-Baptiste Monat
 
### [Markdown & Vous](https://infolit.be/md/) - *Bernard Pochet*

- [Écrire](https://infolit.be/md/ecrire/)
- [Format texte](https://infolit.be/md/formats/)
- [Markdown](https://infolit.be/md/markdown/)
- [YAML](https://infolit.be/md/YAML/)
- [Outils](https://infolit.be/md/outils/)
- [Beamer](https://infolit.be/md/beamer/)
- [Sources](https://infolit.be/md/sources/)
- [À propos](https://infolit.be/md/about/)

### [Débugue tes humanités](https://debugue.ecrituresnumeriques.ca) - *Antoine Fauchié*

Séminaire de la *Chaire de recherche du Canada sur les écritures numériques*. Déjà deux séances cette année (les séances de l'an dernier, vidéos et supports, dans la section [Archives](https://debugue.ecrituresnumeriques.ca/archives/))

#### **Saison 1** (2021 - 11 séances)

- [01 - Introduction](https://debugue.ecrituresnumeriques.ca/seance-01-introduction/ "Séance 01 - Introduction")
  Introduction à la formation

- [02 - L'informatique en 3 points](https://debugue.ecrituresnumeriques.ca/seance-02-informatique/ "Séance 02 - L'informatique en 3 points")
  Origines de l'informatique, principes du numérique et qu'est-ce qu'un programme.

- [03 - Les formats](https://debugue.ecrituresnumeriques.ca/seance-03-formats/ "Séance 03 - Les formats")
  Définitions, implications techniques et politiques, encodage et ouverture.

- [04 - Gérer ses fichiers](https://debugue.ecrituresnumeriques.ca/seance-04-fichiers/ "Séance 04 - Gérer ses fichiers")
  Où sont les fichiers sur un ordinateur, usages basiques d'un terminal, les nuages.

- [05 - Tout savoir sur le terminal](https://debugue.ecrituresnumeriques.ca/seance-05-terminal/ "Séance 05 - Tout savoir sur le terminal")
  Commandes basiques, commandes avancées, grep, bash et oh my zsh.

- [06 - Structurer du texte](https://debugue.ecrituresnumeriques.ca/seance-06-structurer/ "Séance 06 - Structurer du texte")
  Le problème des traitements de texte, baliser le texte, les langages de balisage.

- [07 - Structurer une bibliographie](https://debugue.ecrituresnumeriques.ca/seance-07-bibliographie/ "Séance 07 - Structurer une bibliographie")
  Définition, les styles bibliographiques, introduction à Zotero.

- [08 - Produire des documents](https://debugue.ecrituresnumeriques.ca/seance-08-produire-des-documents/ "Séance 08 - Produire des documents")
  Structuration du texte, introduction à Pandoc et manipulations.

- [09 - Utilisation avancée de Zotero](https://debugue.ecrituresnumeriques.ca/seance-09-zotero-avance/ "Séance 09 - Utilisation avancée de Zotero")
  Organiser sa bibliographie, les groupes, BetterBibTeX.

- [10 - Imprimer des documents](https://debugue.ecrituresnumeriques.ca/seance-10-imprimer/ "Séance 10 - Imprimer des documents")
  Introduction à LaTeX, manipulations, Paged.js.

- [11 - Versionner les fichiers](https://debugue.ecrituresnumeriques.ca/seance-11-versionner/ "Séance 11 - Versionner les fichiers")
  Introduction à Git, exemples de projets, manipulations.

#### **Saison 2** (2022 - 9 séances)

- [01 - Paramétrer son terminal](https://debugue.ecrituresnumeriques.ca/seance-01-parametrer-son-terminal/ "Séance 01 - Paramétrer son terminal")
  Installation de Zsh et de Oh My Zsh, modification de l'apparence. Découverte des fonctions avancées offertes par Zsh. Navigation dans l'historique.

- [02 - Des éditeurs de code et de texte](https://debugue.ecrituresnumeriques.ca/seance-02-editeurs-de-code/ "Séance 02 - Des éditeurs de code et de texte")
  Nano, Vim, EMACS et VSCodium, exploration des possibilités offertes par des approches différentes.

- [03 - Bash ou comment industrialiser le terminal](https://debugue.ecrituresnumeriques.ca/seance-03-bash/ "Séance 03 - Bash ou comment industrialiser le terminal")
  Découverte de Bash, création de quelques scripts, détournements créatifs.

- [04 - Actions à répétition](https://debugue.ecrituresnumeriques.ca/seance-04-actions-a-repetitions/ "Séance 04 - Actions à répétition")
  Renommage de fichiers en lot et autres introductions à la programmation.

- [05 - SSH et les connexions à distance](https://debugue.ecrituresnumeriques.ca/seance-05-ssh/ "Séance 05 - SSH et les connexions à distance")
  Comment rentrer en contact avec d'autres machines, seul·e ou à plusieurs.

- [06 - Sauvegarder ses données](https://debugue.ecrituresnumeriques.ca/seance-06-sauvegarder/ "Séance 06 - Sauvegarder ses données")
  Pourquoi et comment sauvegarder ses données dans un contexte de recherche académique. Principes, organisation et implémentation technique.

- [07 - Git le retour](https://debugue.ecrituresnumeriques.ca/seance-07-git-le-retour/ "Séance 07 - Git le retour")
  Usage réel, paramètres et configuration, gestion des conflits, merge requests, etc.

- [08 - Fabriquer des pages web](https://debugue.ecrituresnumeriques.ca/seance-08-fabriquer-des-pages-web/ "Séance 08 - Fabriquer des pages web")
  Introduction à HTML et CSS, conversions à partir de Markdown avec Pandoc.

- [09 - Fabriquer des sites web](https://debugue.ecrituresnumeriques.ca/seance-09-fabriquer-des-sites-web/ "Séance 09 - Fabriquer des sites web")
  Introduction aux générateurs de site statique, création d'un mini site web depuis un modèle.

#### **Saison 3** - *Littératie numérique* (2023, en cours)

- [01 - Comprendre l'ordinateur avec le terminal](https://debugue.ecrituresnumeriques.ca/seance-01-gerer-les-fichiers/ "Séance 01 - Comprendre l'ordinateur avec le terminal")
  Où sont les fichiers sur un ordinateur. Le terminal.

- [02 - Gérer les fichiers avec le terminal](https://debugue.ecrituresnumeriques.ca/seance-02-le-terminal/ "Séance 02 - Gérer les fichiers avec le terminal")
  Le terminal. Commandes basiques et avancées.

- 03 - Les formats des fichiers - *2023-02-14*
  Définitions et usages. Markdown, HTML, PDF.

- 04 - Git - *2023-03-28*
  Le versionnement des documents.

- 05 - Zotero - *2023-04-25*
  Usages avancés de Zotero.

- 06 - Introduction à l'éditeur de texte Stylo - *2023-05-16*
  Un éditeur de texte pour les sciences humaines.

- 07 - Produire des documents - *2023-06-13*
  Stylo, Zettlr, Pandoc. Comment convertir des fichiers en différents formats.

#### **Saison 3** - *Avancé* (2023, en cours)

- [01 - Les langages de balisage](https://debugue.ecrituresnumeriques.ca/seance-01-langages-de-balisage/ "Séance 01 - Les langages de balisage")
  De Markdown à HTML et XML. Le balisage sémantique.

- 02 - Introduction au CSS - *2023-03-14*
  Pour mettre en forme les documents au format HTML.

- 03 - Introduction à Javascript - *2023-04-11*
  Pour développer des interactions sur le Web.

- 04 - Générateur de site statique (1/2) - *2023-05-09*
  Comment transformer des fichiers textes en page Web HTML.

- 05 - Générateur de site statique (2/2) - *2023-05-30*
  Le déploiement d'un site sur le Web grâce au service GitHub Pages.

- 06 - paged.js - *2023-06-20*
  Faire du Web to print et transformer des contenus HTML en PDF depuis un navigateur.




### Petits guides / tutos

- [Pandoc Markdown vs standard Markdown](https://www.speakon.org.uk/MarkupBinder/beta/docs/Markdown/Pandoc_Markdown_vs_standard_Markdown.html)
- [Everything pandoc Markdown can do](https://benjaminwuethrich.dev/2020-05-04-everything-pandoc-markdown.html) - Benjamin Wuethrich - *ArtifiShell Intelligence* - 2020-05-04 -- Un abrégé (déjà long) du Markdown saveur Pandoc.
- [Du LaTeX sans LaTeX avec Pandoc !](https://www.unixmail.fr/informatique/latex-sans-latex-avec-pandoc/) - Zilkos - 2016-03-11
- [Rédiger et manipuler des documents avec Markdown](https://golb.statium.link/post/20190528rediger-et-manipuler-des-documents-markdown/) - Christophe Masutti - *Statium Blog* - 2019-05-28 (citeproc est désormais installé par défaut dans pandoc)
- [Convert Docx To Markdown With Pandoc](https://ronn-bundgaard.dk/blog/convert-docx-to-markdown-with-pandoc/) - Martin - *Rønn Bundgaard Blog* - 2015-03-24
- [How to Convert from Latex to MS Word with ‘Pandoc’](https://medium.com/@zhelinchen91/how-to-convert-from-latex-to-ms-word-with-pandoc-f2045a762293) - Zhelin Chen - *Medium* - 2019-02-25
- [How to use Pandoc to produce a research paper](https://opensource.com/article/18/9/pandoc-research-paper) - Kiko Fernandez-Reyes - *opensource.com* - 2018-10-19
- [Importing Markdown in InDesign](https://github.com/jgm/pandoc/wiki/Importing-Markdown-in-InDesign) - Carlos Precioso - Wiki github officiel - 2020-01-20



## *Academic ressources*

### Articles

- [Markdown comme condition d’une norme de l’écriture numérique](http://www.reel-virtuel.com/numeros/numero6/sentinelles/markdown-condition-ecriture-numerique) - Antoine Fauchié - *Réel_Virtuel* #6 - 2018
- [Markdown for Librarians and Academics](https://www.tandfonline.com/doi/abs/10.1080/01639269.2014.904696) - Steven Ovadia - *Behavioral & Social Sciences Librarian* - 2014-05-22 - DOI:10.1080/01639269.2014.904696
- [Formatting Open Science: agilely creating multiple document formats for academic manuscripts with Pandoc Scholar](https://peerj.com/articles/cs-112/) - Albert Krewinkel, Robert Winkler - *PeerJ Computer Science* - 2017-05-08 - DOI:10.7717/peerj-cs.112
- [Open collaborative writing with Manubot](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1007128) - Daniel S. Himmelstein, Vincent Rubinetti, David R. Slochower, Dongbo Hu, Venkat S. Malladi, Casey S. Greene, Anthony Gitter - _PLOS Computational Biology_ - 2019-06-24 - DOI:10.1371/journal.pcbi.1007128
- [Reproducible manuscript preparation with RMarkdown application to JMSACL and other Elsevier Journals](https://www.sciencedirect.com/science/article/pii/S2667145X21000171) - Daniel T. Holmes, Mahdi Mobini, Christopher R. McCudden - *Journal of Mass Spectrometry and Advances in the Clinical Lab* - 2021-09-02 - DOI:10.1016/j.jmsacl.2021.09.002

### Blogs

#### Billets du [blog](https://www.arthurperret.fr) d'*Arthur Perret*

- [Une micro-chaîne éditoriale multicanal pour l’écriture scientifique](https://www.arthurperret.fr/blog/2018-10-18-une-micro-chaine-editoriale.html) - 2018-10-18
- [Sémantique et mise en forme, ouvrir la boîte de **Pandoc** ?](https://www.arthurperret.fr/blog/2018-12-04-semantique-et-mise-en-forme.html) - 2018-12-04
- [Le diaporama au temps du balisage (2)](https://www.arthurperret.fr/blog/2019-10-07-le-diaporama-au-temps-du-balisage-2.html) - 2019-10-07
- [Citations marginales](https://www.arthurperret.fr/blog/2019-12-17-citations-marginales.html) - 2019-12-17
- [Ancres et liens](https://www.arthurperret.fr/blog/2020-01-02-ancres-et-liens.html) - 2020-01-02
- [Écrire et éditer](https://www.arthurperret.fr/blog/2020-05-22-ecrire-et-editer.html) - 2020-05-22
- [Dr Pandoc & Mr Make](https://www.arthurperret.fr/blog/2020-09-14-dr-pandoc-and-mr-make.html) - 2020-09-14
- [Enseignement et automatisation avec Pandoc](https://www.arthurperret.fr/blog/2020-10-19-enseignement-automatisation-pandoc.html) - 2020-10-19
- [Sémantique et mise en forme (2) : consolider les fondations](https://www.arthurperret.fr/blog/2021-12-01-semantique-et-mise-en-forme-2.html) - 2021-12-01
- [Liens wiki et identifiants](https://www.arthurperret.fr/blog/2022-06-15-liens-wiki-et-identifiants.html) - 2022-06-15
- [Publication multiformats avec Pandoc et Make](https://www.arthurperret.fr/blog/2022-06-22-publication-multiformats-pandoc-make.html) - 2022-06-22
- [Markdown et traitement de texte : une méthode pour collaborer](https://www.arthurperret.fr/blog/2022-10-25-markdown-traitement-de-texte-methode-pour-collaborer.html) - 2022-10-25

#### Sur le [blog](https://www.quaternum.net/carnet/) d’*Antoine Fauchié*

##### [Les fabriques de publication](https://www.quaternum.net/2020/04/29/les-fabriques-de-publication/)

1. [LaTeX](https://www.quaternum.net/2020/04/29/fabriques-de-publication-latex/)
2. [Pandoc](https://www.quaternum.net/2020/04/30/fabriques-de-publication-pandoc/)
3. [Asciidoctor](https://www.quaternum.net/2020/05/03/fabriques-de-publication-asciidoctor/)
4. [Jekyll](https://www.quaternum.net/2020/05/05/fabriques-de-publication-jekyll/)
5. [Org-Mode](https://www.quaternum.net/2020/05/10/fabriques-de-publication-org-mode/)
6. [Quire](https://www.quaternum.net/2020/05/13/fabriques-de-publication-quire/)
7. [Gabarit Abrupt](https://www.quaternum.net/2020/05/18/fabriques-de-publication-gabarit-abrupt/)
8. [Stylo](https://www.quaternum.net/2020/05/21/fabriques-de-publication-stylo/)
9. [Zettlr](https://www.quaternum.net/2020/08/28/fabriques-de-publication-zettlr/)

#### Sur le Carnet hypothèses de **Eveille** (en cours)

- [Histoire et philosophie du markdown](https://eveille.hypotheses.org/5990)
- [Syntaxe du markdown](https://eveille.hypotheses.org/6004)
- [Zettlr, un outil pour l’écriture académique](https://eveille.hypotheses.org/6019)
- [\[Ressources\] Markdown](https://eveille.hypotheses.org/6035)
- [Yaml](https://eveille.hypotheses.org/6050)

#### Autres billets sur Pandoc

- [Pandoc : un couteau suisse de l’édition](https://carnet.fabriquedunumerique.org/pandoc-un-couteau-suisse-de-ledition/) - Emmanuelle Lescouet - *Le carnet de La Fabrique du Numérique* - 2023-02-13
- [Pandoc — convert Markdown to HTML using a custom template](https://vimalkvn.com/pandoc-markdown-to-html/) - Vimalkumar Velayudhan - 2020-06-22
- [Customizing pandoc to generate beautiful pdf and epub from markdown](https://learnbyexample.github.io/customizing-pandoc/) - learnbyexample.github.io - 2020-07-21
- [Pandoc et Métopes](https://etnadji.fr/blog/pandoc-metopes.html) - Ét.Nadji.fr - 2019-07-04

### Templates

- Dans le wiki officiel : [User contributed templates](https://github.com/jgm/pandoc/wiki/User-contributed-templates)
- [Gabarit Abrüpt](https://gitlab.com/cestabrupt/gabarit-abrupt)
- [Pandoc Scholar](https://pandoc-scholar.github.io/) (n’est plus maintenu, les auteurs conseillent de se tourner vers *Quarto*)
- [Eisvogel](https://github.com/Wandmalfarbe/pandoc-latex-template) : A clean pandoc $LaTeX$ template to convert your markdown files to PDF or $LaTeX$.
- [Pandoc worflow](https://github.com/CERES-RUB/er-pandoc-workflow), templates html et tex, scripts shell de la revue *Entangled Religions* (en allemand)
- [A simple letter template for Pandoc](https://github.com/aaronwolen/pandoc-letter) - Aaron Wolen and Andrew Dunning

### Exemples

- Robin de Mourat - Thèse - [Le vacillement des formats Matérialité, écriture et enquête : le design des publications en Sciences Humaines et Sociales](http://www.these.robindemourat.com/)

- Arthur Perret - Thèse - [De l’héritage épistémologique de Paul Otlet à une théorie relationnelle de l’organisation des connaissances](https://these.arthurperret.fr/)

- Djéban Landry Koffi - Mémoire de Master - [L’écriture de « l’analphabétisme » dans le récit autobiographique L’analphabète d’Agota Kristof : Une expression d’un exil](https://master2-kristof.netlify.app/)
  - [Version pdf](https://master2-kristof.netlify.app/data/memoire-djeban-landry-koffi.pdf)
  - Sources ([Gitlab UPPA](https://git.univ-pau.fr/jrabaud001/memoire-djeban-landry-koffi))

## Editeurs de texte

### Zettlr

- Pandoc intégré
- Options Pandoc gérées dans [Assets Manager](https://docs.zettlr.com/en/core/assets-manager/)
- [Zettlr : markdown puissance dix](https://golb.statium.link/post/20200218zettlr/) - Christophe Masutti - *Statium Blog* - 2020-02-18

#### Billets sur Zettlr

- Arthur Perret : [Recommandation : Zettlr](https://www.arthurperret.fr/blog/2019-09-06-recommandation-zettlr.html) - 2019-09-06
- Aurore Turbiau : [Rédiger la thèse avec Zettlr](https://engagees.hypotheses.org/2948) - Carnet _Littératures engagées_ - 2021-04-05

### Obsidian

#### Ressources

- [Pandoc, Obsidian, la fac & zotero](https://www.mara-li.fr/outils/tutoriel/Pandoc%2C%20Obsidian%2C%20la%20fac%20%26%20zotero/) - *Seed* - 2023-01-02
- [Using Pandoc inside Obsidian](https://publish.obsidian.md/hub/04+-+Guides%2C+Workflows%2C+%26+Courses/Guides/Using+Pandoc+inside+Obsidian) - *SkepticMystic* sur [*Obsidian Hub*](https://publish.obsidian.md/hub/00+-+Start+here) - 
- Version *video*  avec chapitrage : [Pandoc and Obsidian - Create slideshows, PDFs and Word documents](https://publish.obsidian.md/hub/04+-+Guides%2C+Workflows%2C+%26+Courses/Community+Talks/YT+-+Pandoc+and+Obsidian+-+Create+slideshows%2C+PDFs+and+Word+documents) - *SkepticMystic* - *[Obsidian Community Talks](https://publish.obsidian.md/hub/01+-+Community/Events/Obsidian+Community+Talks)*

#### Plugins Pandoc--Obsidian

- [Pandoc Reference List](https://github.com/mgmeyers/obsidian-pandoc-reference-list) 
- [Obsidian Enhancing Export Plugin](https://github.com/mokeyish/obsidian-enhancing-export)
  - Meilleur que *Pandoc Plugin*


### VS Code / VS Codium

#### [Pandoc Markdown](https://github.com/rwildcat/vsc-pandoc-markdown)

A VS Code extension for writing (with live preview) Markdown documents using [Pandoc](https://pandoc.org/).

##### Features

-   Fully supports all Pandoc extensions (metadata, bibliography, etc.)
-   Provides full Pandoc live previewing
-   Provides an enhanced _Article_ (CSS) style
-   Allows user-defined extra Pandoc command line options
-   Provides a function to exports to HTML
-   Access to local media from rendered preview panel (images, etc.)
-   Fully configurable via header YAML metadata



##### Provides

-   `Pandoc Markdown: Open Preview` - (<kbd>Ctrl</kbd><kbd>Shft</kbd><kbd>R</kbd> Windows / <kbd>Cmd</kbd><kbd>Shft</kbd><kbd>R</kbd> Mac) Renders current MD file in editor using Pandoc and opens/refreshes live preview panel.
-   `Pandoc Markdown: Export to HTML` - Exports current MD file in editor to an HTML file. Result will be located in the same directory as source file, same base name.
-   `article.css` - a CSS file for fine-tuning and extending Pandoc's defaults. Provides centered abstract, tables and figures; less contrasting font sizes, and a new HTML element, `<aside>`, for displaying figures, text and other contents as an insert to the right side of the page column.

## [Pandoc Extras](https://github.com/jgm/pandoc/wiki/Pandoc-Extras) (*Build on Pandoc*)

### *Static Site Generators*

- [Pandoc as a site generator](https://ordecon.com/2020/07/pandoc-as-a-site-generator/index.html)
- [Gitit](https://github.com/jgm/gitit) - Gestionnaire de wiki par le créateur de Pandoc
- [Migrating from Wordpress to Jekyll](https://heidloff.net/article/migrating-from-wordpress-to-jekyll/) - Niklas Heidloff - 2023-01-16

### [Quarto](https://quarto.org)

> Quarto® is an open-source scientific and technical publishing system built on [Pandoc](https://pandoc.org/)
>
> -   Create dynamic content with [Python](https://quarto.org/docs/computations/python.html), [R](https://quarto.org/docs/computations/r.html), [Julia](https://quarto.org/docs/computations/julia.html), and [Observable](https://quarto.org/docs/computations/ojs.html).
> -   Author documents as plain text markdown or [Jupyter](https://jupyter.org/) notebooks.
> -   Publish high-quality articles, reports, presentations, websites, blogs, and books in HTML, PDF, MS Word, ePub, and more.
> -   Author with scientific markdown, including equations, citations, crossrefs, figure panels, callouts, advanced layout, and more.



### [Pandiff](https://github.com/davidar/pandiff)

-   Prose diffs for [any document format supported by Pandoc](https://pandoc.org/MANUAL.html)
-   Supported output formats:
    -   [CriticMarkup](http://criticmarkup.com/)
    -   HTML
    -   PDF, via LaTeX
    -   [Word docx](https://en.wikipedia.org/wiki/Office_Open_XML) with [Track Changes](https://support.office.com/en-us/article/track-changes-in-word-197ba630-0f5f-4a8e-9a77-3712475e806a)
-   Respects document structure, so won't produce broken markup like `wdiff`