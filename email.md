
<!-- ce message est en markdown "brut" : pour voir un rendu plus lisible vous pouvez utiliser https://stackedit.io/ , un éditeur markdown en ligne : cliquez sur "start writing" (tout en haut) et remplacez le texte en markdown par celui-ci -->  
  
Bonjour,  
  
Vous recevez ce message car vous êtes inscrits à l'Atelier [**Markdown, Zotero, Pandoc : écrire, citer, exporter**](https://www.stretchingnumerique.fr/ressources/markdown-zotero-pandoc-ecrire-citer-exporter-complet) qui aura lieu ce **mardi 21 mars de 10h à 11h30**.  
  
### Connexion  
  
L'atelier aura lieu sur *Teams* (Solution institutionnelle de l'UPPA) à cette adresse :  
- `https://teams.microsoft.com/l/meetup-join/XXXXX`
  
### Pré-requis  
  
#### Un éditeur de texte  
  
Le *bloc-note* peut suffire mais vous n'aurez pas les fonctionnalités d'un éditeur de texte plus complet (coloration syntaxique, prévisualisation, curseurs multiples...). Je vous recommande donc, au choix ou les deux :  
  
- un éditeur **spécialisé** en markdown : [Zettlr](https://www.zettlr.com/) (libre)  
- un éditeur **généraliste** et extensible : [Visual Studio Code](https://code.visualstudio.com/) (Microsoft - gratuit) ou sa version "dé-microsoftisée" [VSCodium](https://vscodium.com/)  
  
#### [Better BibTeX for Zotero](https://retorque.re/zotero-better-bibtex/)  
  
Une extension qui permet, entre autre, de :  
  
- Gérer les clés de citations des références dans Zotero  
- Garder synchronisés les fichiers textes de références bibliographiques exportés au formats bibtex (.bib) ou csl-json (.json) dont se servira *Pandoc* pour la création des fichiers word, pdf, html...  
  
> Guide d'installation : <https://retorque.re/zotero-better-bibtex/installation/>  
  
### [Pandoc](https://pandoc.org/installing.html)  
  
"Couteau suisse de l'édition", en ligne de commande.  
  
(Zettlr intègre sa propre version de Pandoc, qui n'est pas utilisable en dehors de Zettlr)  
  
#### Une distribution $LaTeX$  
  
Pandoc utilise $LaTeX$ pour générer les pdf.  
  
Les recommandations et conseils du développeur de Zettlr ici : [Installer LaTeX](https://docs.zettlr.com/fr/installing-latex/)  
  
### Quelques liens utiles  
  
- [Tutoriel markdown interactif et multilingue](https://markdowntutorial.com/)  
- La [Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/) du site *Markdown Guide*  
- [La section *Pandoc's Markdown* du Manuel Pandoc](https://pandoc.org/MANUAL.html#pandocs-markdown)  
  
Vous souhaitant une bonne fin de semaine,  
Cordialement,  
  
> Julien Rabaud  
> *Bibliothécaire assistant spécialisé*  
  
> Service Commun de la Documentation  
> *Service d'appui à la recherche*  
> Bibliothèque Universitaire de Pau  
> Campus Universitaire  
> BP 7506 - 64075 Pau Cedex  
  
> [@UjuBib sur Twitter](https://twitter.com/ujubib)