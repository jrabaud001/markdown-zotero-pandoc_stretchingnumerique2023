---
title: "Markdown, Zotero, Pandoc : écrire, citer, exporter"
author: Julien Rabaud
date: 2023-03-21
theme: "solarized"
transition: "slide"
logoImg: "img/LogoStretchingNoir_0.png"
slideNumber: true
center: true
pdfSeparateFragments: false
previewLinks: true
preloadIframes: true
---

<style type="text/css">
@import url('https://fonts.googleapis.com/css2?family=Lobster&display=swap');
h1, h2, h3, h4, h5, h6 {
    font-family: "Lobster" !important;
    /* font-weight: 600 !important;
    letter-spacing:-1px !important; */
    text-transform: none !important;
    color: DeepPink !important;
}
em {
    color: black !important;
}
strong {
  color: black !important;
}
section li {
  list-style-type: "⟶ ";
  color: saddlebrown;
  font-family: "calibri" !important;
}
section li li {
  list-style-type: "››› ";
}
section li li li {
  list-style-type: "- ";
}
code {
  color: Indigo;
  font-weight: 600;
}
#logo img {
  max-height: 200px !important;  
  height: 140px !important;
}
.petit{
  font-size:0.7em;
}
.discl {
  font-size: 0.6em;
  color: hotpink;
  border: thick double hotpink;
  margin-right:2em;
  margin-top:1em;
  padding: 0.5em;
}
.sources {
  left:0;
  right:0;
  margin-left: auto;
  margin-right: auto;
  margin-top: 3em;
  position: absolute;
  width: fit-content;
  display:block;
  text-align:center;
  font-size: 0.5em;
  font-family: monospace;
  color: deeppink;
  border: thick double pink;
  padding: 0.5em;
}
</style>

***
##### Stretching Numérique 2023
***

![]()

### `Atelier` 
### Markdown, Zotero, Pandoc :
### *écrire, citer, exporter*

![]()

***
Julien Rabaud (*Bibas* - *SCD UPPA*)  
[@UjuBib](https://twitter.com/ujubib) // [@UjuBib@mamot.fr](https://mamot.fr/@UjuBib) {.petit}
***

--

<!-- 
[![](https://mermaid.ink/img/pako:eNp9VN1u2jAUfhXLEiNowRNtoZCLStV6WaZq1XYxZZqc-AAejp3ZzgAhHojn4MVmOwFCxXoR2-f4O9_58cnZ4lwxwAnudLapRIhLbhMUjgh17QIK6Caom1ED3bit_U41p5kA0z3B3RWjejl1hE45o8JAfbHzm1t2nU4q55qWC_T8NZVea6qsVhz2uea20sGEs0E0dVRMrWTPK0CyN3h92M8Oew0yB9P_oSxoVVveP5CMZzEiv42SbtvQQhBCfl6n-azkX9CGK1lb30TRC5VM5b2WX_smUsbtyeA2Ig697jVSFhHFbCPdRcTC-j8JvFSZ4Dk9Mw0jsrCFaGxHESnZrBEmEYGyyi6ZOBtvtx8RyY1xaRKy2x31g37_waXS1KMt3AThFn0IsYbtLqzjhhEFwDDoJk0WQTXy5J2O4CCRrxgSgAoqKxCoLpgH5y6jpXeDUlwGLVF6_mn6-OXb43PILsXoVyaoXDbVsBvHc3p7NONCJILPF3auAVqQ8zu1MCX3PGeW-l1agExUcAa0Kl5j5kqw8_WVjqphWaXFZqUUa8fsk8yVUDrxTmJjtVpCwgBKH1Uj91ec2UUyKtdtwwFq0AZoSPNd9P0R7WpU-Ja-AN9egsdHsP8RXUX_VIobuGJS93QuqDFPMEPATl6UljOhVqBbeV06C1a-gWLfRLFvIWhqcyJ03doORWkq5--zDR3RyH0Tb4tjXIAuKGduNIXxkuIwdlKcuKOnTLEbKA5HK6teNzLHidUuYFyVjFp44q60tMBJGEO7f0Ics40?type=png)](https://mermaid.live/edit#pako:eNp9VN1u2jAUfhXLEiNowRNtoZCLStV6WaZq1XYxZZqc-AAejp3ZzgAhHojn4MVmOwFCxXoR2-f4O9_58cnZ4lwxwAnudLapRIhLbhMUjgh17QIK6Caom1ED3bit_U41p5kA0z3B3RWjejl1hE45o8JAfbHzm1t2nU4q55qWC_T8NZVea6qsVhz2uea20sGEs0E0dVRMrWTPK0CyN3h92M8Oew0yB9P_oSxoVVveP5CMZzEiv42SbtvQQhBCfl6n-azkX9CGK1lb30TRC5VM5b2WX_smUsbtyeA2Ig697jVSFhHFbCPdRcTC-j8JvFSZ4Dk9Mw0jsrCFaGxHESnZrBEmEYGyyi6ZOBtvtx8RyY1xaRKy2x31g37_waXS1KMt3AThFn0IsYbtLqzjhhEFwDDoJk0WQTXy5J2O4CCRrxgSgAoqKxCoLpgH5y6jpXeDUlwGLVF6_mn6-OXb43PILsXoVyaoXDbVsBvHc3p7NONCJILPF3auAVqQ8zu1MCX3PGeW-l1agExUcAa0Kl5j5kqw8_WVjqphWaXFZqUUa8fsk8yVUDrxTmJjtVpCwgBKH1Uj91ec2UUyKtdtwwFq0AZoSPNd9P0R7WpU-Ja-AN9egsdHsP8RXUX_VIobuGJS93QuqDFPMEPATl6UljOhVqBbeV06C1a-gWLfRLFvIWhqcyJ03doORWkq5--zDR3RyH0Tb4tjXIAuKGduNIXxkuIwdlKcuKOnTLEbKA5HK6teNzLHidUuYFyVjFp44q60tMBJGEO7f0Ics40)
-->

![](img/mermaid-diagram-2023-03-20.svg)

---

<!-- .slide: data-background="https://64.media.tumblr.com/tumblr_lyfuwzuR0K1qh3ft3o1_500.gif" -->

## Généralités

--

### Fichiers binaires 

***

- Nécessitent un **logiciel spécifique** (souvent avec license) pour être lus et édités : 
  - `.docx`, `.odt`, `.xslx`, `.pdf`, `.jpg`

***

_Exemple_ : dézipper un fichier `.docx`

--

### Fichiers textes

***

- Lisibles et modifiables dans un **simple éditeur** de texte : 
  - `.md`, `.html`, `.tex`, `.csv`, `.json`, `.bib`, `.css`, `.py`, `.xml`, `.svg`...



***
- Peuvent être manipulés par des **utilitaires standards** du monde libre, très puissants : 
    - `awk`, `sed`, `grep`, `git`, `bash`, ...

***

_Exemple_ : [Inventaire Blot](https://inventaire-blot.netlify.app/#markdown)

--

***

`Fond` - `Structuration` - `Balisage`

***

*Forme* - *Composition* - *Feuille de style*

***

<span class="discl">/!\ Je ne suis pas sémiologue</span> {.fragment .fade-right}

---

## Markdown

--

#### Définition

> **Markdown** est un langage de balisage léger créé en 2004 par **John Gruber** avec l'aide d'**Aaron Swartz**. Il a été créé dans le but d'offrir une **syntaxe facile à lire et à écrire**.  
>
> Un document balisé par Markdown peut être lu en l'état sans donner l’impression d'avoir été balisé ou formaté par des instructions particulières. {style=background-color:mistyrose;color:sienna;text-align:justify;font-size:0.66em;padding:.5em}

Wikipedia (fr) : [Markdown](https://en.wikipedia.org/wiki/Markdown) {style=font-size:0.5em;text-align:right;margin-right:130px;}

***

\> [Le site de John Gruber](https://daringfireball.net/projects/markdown/) {style=font-size:0.6em;text-align:left;margin-left:150px;}

\> [Le billet du regretté Aaron Swartz](http://www.aaronsw.com/weblog/001189) {style=font-size:0.6em;text-align:left;margin-left:150px;}

***

#CultureNum : documentaire [The Internet's Own Boy: The Story of Aaron Swartz](https://peertube.mxinfo.fr/videos/watch/8b676d61-0be2-46e4-929f-27f482492f8b)  
(sous-titres en plusieurs langues disponibles) {style=font-size:0.4em;text-align:right;margin-right:130px;margin-left:150px;}

--

### Saveurs 

  - CommonMark (*ppdc*)
  - Github Flavored Markdown (*GFM*)
  - MultiMarkdown (*MacOS, révision*)
  - **Pandoc's Markdown**
  - Rmarkdown (`.rmd`)
  - ...

--

#### Syntaxe `inline`

 Markdown | html | Affichage {style=font-size:0.8em;}
---|---|---
`*italique*` ou `_italique_`|`<em>italique</em>`|*italique* {style=font-size:0.7em;}
`**gras**`|`<strong>gras<strong>`|**gras** {style=font-size:0.7em;}
`***gras italique***`|`<strong><em>gras italique</em></strong>`|***gras italique*** {style=font-size:0.7em;}
`~~barré~~`|`<s>barré</s>`|~~barré~~ {style=font-size:0.7em;}
`XIX^e^`|`XIX<sup>e</sup>`|XIX<sup>e</sup> {style=font-size:0.7em;}
`H~2~O`|`H<sub>2</sub>O`|H<sub>2</sub>O {style=font-size:0.7em;}
`[texte](url)`|`<a href="url">texte</a>`|<a href="url">texte</a> {style=font-size:0.7em;}
`[texte]{.style}`|`<span class="style">texte</span>`|texte {style=font-size:0.7em;}
||

--

#### Liens à retenir pour apprendre *Markdown*

- [Tutoriel interactif multilingue](https://markdowntutorial.com)
- [Section Markdown](https://pandoc.org/MANUAL.html#pandocs-markdown) du *Manuel Pandoc*
- [Markdown Guide](https://markdownguide.org)<span style="color:hotpink">(\*)</span>. Contient :
  - [Markdown Cheat Sheet](https://markdownguide.org/cheat-sheet)
    - [Basic Syntax](https://markdownguide.org/basic-syntax)
    - [Extended Syntax](https://markdownguide.org/extended-syntax)
  - \[[Tools](https://markdownguide.org/tools) - Liste de logiciels qui utilisent *markdown*.\]

***
(\*) Ne traite pas des références bibliographiques, spécificité de **Pandoc**.  
Voir [Manuel Pandoc \> Citations](https://pandoc.org/MANUAL.html#citations) {style=font-size:0.6em;color:hotpink}

--

#### Des logiciels qui l'implémentent

- *Framanotes* (fermé)
  - (Instance de [Turtle](https://github.com/turtl) - alternative à *Pocket*)
- *Zotermite* (R. de Mourat) - \[[sources](https://github.com/robindemourat/zotermite)\]
- Les notes dans *[Vivaldi](https://vivaldi.com/fr/)* (navigateur web, non-libre), 
- La zone de transcription dans *[Tropy]*, 
- Les fichiers `README.md` des dépôts *Github*...

[Tropy]: https://docs.tropy.org/in-the-item-view/notes

--

#### Aujourd'hui encore plus

- le nouveau gestionnaire de notes de *Zotero*
  - [Zotero 6 : transformer votre flux de travail de recherche](https://zotero.hypotheses.org/4145) {.petit}
- *GoogleDocs* en saisie
  - [Utiliser Markdown dans Google Docs, Slides et Drawings](https://support.google.com/docs/answer/12014036?hl=fr) {.petit}
- conversations dans *Microsoft Teams*
  - [Utiliser la mise en forme Markdown dans Teams](https://support.microsoft.com/fr-fr/office/utiliser-la-mise-en-forme-markdown-dans-teams-4d10bd65-55e2-4b2d-a1f3-2bebdcd2c772) {.petit}
- *Stylo*, l'éditeur d'articles de revue de `Huma-Num`
  - [Écrire les SHS en environnement numérique. L’éditeur de texte Stylo](https://intelligibilite-numerique.numerev.com/numeros/n-1-2020/18-ecrire-les-shs-en-environnement-numerique-l-editeur-de-texte-stylo) <br> *Vitali-Rosati M.*, *Sauret N.*, *Fauchié A.* & *Mellet M.* - Intelligibilité du numérique n°1, 2020. {.petit}


---

### éditeurs de texte

--

#### VSCode

éditeur de code généraliste et extensible

- Extensions *Pandoc* :
  - [vscode-markdown-it-pandoc](https://github.com/ickc/vscode-markdown-it-pandoc) pour la coloration
  - [vsc-pandoc-markdown](https://github.com/rwildcat/vsc-pandoc-markdown) pour prévisualiser
  - [vscode-pandoc](https://github.com/dfinke/vscode-pandoc)
    pour exporter
- Diaporama (via [reveal.js](https://revealjs.com/))
  - [vscode-reveal](https://github.com/tokiedokie/reveal-markdown)
- Mindmap (via [markmap.js](https://markmap.js.org/))
  - [markmap-vscode](https://github.com/markmap/markmap-vscode)


--

#### Zettlr

éditeur markdown pour l'écriture (*notes* et *rédaction*) scientifique (*gestion des références*)

- [Site officiel](https://www.zettlr.com/)
- [Documentation](https://docs.zettlr.com)

--

#### Stylo

éditeur en ligne d'articles scientifiques

- [Documentation officielle](http://stylo-doc.ecrituresnumeriques.ca/fr_FR/#!index.md)
- Avoir un compte *huma-num*
- https://stylo.huma-num.fr/


---

### Configurer <br> _Better BibTeX for Zotero_

--

1- Exporte une collection Zotero dans un fichier `.json` ou `.bib` et garde le fichier synchronisé avec la collection. {style=color:DeepPink;font-size:0.5em;text-align:left;}

<table> 
  <tr>
  <td style="text-align:center;line-height:4px;border-bottom:0px">
      <img src="img/ZoteroExportColl1.png" height=90px style="border: 0px" onclick="window.open('img/ZoteroExportColl1.png','_blank')"/><br/>
      <span style='font-size:0.3em;line-height:0.4em'>Mettre toute ses références dans une collection. <br>Puis clic-droit : <code style="padding:1px;border-radius:0px">Exporter la collection</code></span>
    </td>
    <td style="text-align:center;line-height:4px;border-bottom:0px">
      <img src="img/ZoteroExportColl2.png" height=90px style="border: 0px" onclick="window.open('img/ZoteroExportColl2.png','_blank')"/><br/>
      <span style='font-size:0.3em;line-height:0.5em'>Choisir le format <code style="padding:1px;border-radius:0px">Better CSL JSON</code> ou <code style="padding:1px;border-radius:0px">Better BibTeX</code>...<br/>Et cocher <strong>Garder à jour</stron></span>
    </td>
    <td style="text-align:center;line-height:4px;border-bottom:0px">
      <img src="img/ZoteroExportColl3.png" height=90px style="border: 0px" onclick="window.open('img/ZoteroExportColl3.png','_blank')"/><br/>
      <span style='font-size:0.3em;line-height:0.7em'>Exporter dans le dossier <code style="padding:1px;border-radius:0px">data</code> du répertoire d'écriture</span>
    </td>
  </tr>
</table> 

***

2- Ajoute un convertisseur pour le *Quick Copy* (<kbd>ctrl</kbd>+<kbd>shift</kbd>+<kbd>c</kbd>) qui met dans le presse-papier `[@clédelaréf]` {style=color:DeepPink;font-size:0.5em;text-align:left;}

<table>
  <tr>
    <td style="text-align:center;line-height:4px;border-bottom:0px">
      <img src="img/ZoteroBetterBibTeXPandoc2.png" height=150px style="border: 0px" onclick="window.open('img/ZoteroBetterBibTeXPandoc2.png','_blank')"/><br/>
      <span style='font-size:0.3em;line-height:0.5em;'>Choisir <code style="padding:1px;border-radius:0px">Better BibTeX [@citekeys] Quick Copy</code> comme format de sortie par défaut</span>
    </td>
    <td style="text-align:center;line-height:4px;border-bottom:0px">
      <img src="img/ZoteroBetterBibTeXPandoc.png" height=150px style="border: 0px" onclick="window.open('img/ZoteroBetterBibTeXPandoc.png','_blank')"/><br/>
      <span style='font-size:0.3em;line-height:0.5em'>Paramétrage des préférences de BetterBibTeX</span>
    </td>
  </tr>
</table>

***
- [Documentation officielle pour Markdown/Pandoc](https://retorque.re/zotero-better-bibtex/exporting/pandoc/) {style=font-size:0.7em;text-align:left;}

---

## Atelier `Article`

---

## Pandoc

--

#### Word 

- avec bibliographie et sommaire

```bash
pandoc articleFauchie.md \
--toc \
--toc-depth=3 \
-C \
--bibliography=data/Article-Fauchie.json \
--csl=data/iso690-author-date-fr.csl \
-o sorties/articlesFauchie_ISO690.docx
```

--

#### Word 

- avec un document modèle et un autre style csl

```sh
pandoc articleFauchie.md \
--toc \
--toc-depth=3 \
--reference-doc=data/styleBraud.docx \
-C \
--bibliography=data/Article-Fauchie.json \
--csl=data/transversalites-Braud.csl \
-o sorties/articlesFauchie_styleBraud.docx
```

- `--reference-doc=FILE`

--

#### Html

```sh
pandoc articleFauchie.md \
-s \
--embed-resources \
--toc \
--toc-depth=3 \
-C \
--bibliography=data/Article-Fauchie.json \
--csl=data/iso690-author-date-fr.csl \
-o sorties/articleHTMLsimple.html
```

- `-s` (*Standalone*)
- `--embed-resources`

--

#### Html

- avec css (bleu biblio à droite)

```sh
pandoc articleFauchie.md \
-s \
--embed-resources \
--include-in-header=data/bleu.css \
--toc \
--toc-depth=3 \
-C \
--bibliography=data/Article-Fauchie.json \
--csl=data/iso690-author-date-fr.csl \
-o sorties/articleHTML_bleu.html
```
- `--include-in-header=FILE`

--

#### html 

- avec css (orange, notes à droite)

```sh
pandoc articleFauchie.md \
-s \
--embed-resources \
--reference-location=block \
--include-in-header=data/orange.css \
--toc \
--toc-depth=3 \
-C \
--bibliography=data/Article-Fauchie.json \
--csl=data/iso690-author-date-fr.csl \
-o sorties/articleHTML_orange.html
```

--

### Pdf


```sh
pandoc articleFauchie.md \
--pdf-engine=xelatex \
--metadata notes-after-punctuation=false \
--toc \
--toc-depth=3 \
--reference-doc=data/styleBraud.docx \
-C \
--bibliography=data/Article-Fauchie.json \
--csl=data/transversalites-Braud.csl \
-o sorties/articlePDF.pdf
```

- Avec liste des figures :
  - Ajouter une ligne dans l'entête YAML : `lof: true`
- `--metadata`  
  `notes-after-punctuation=false`

--

### "Default file"

Un *fichier unique* (au format `YAML`) avec toutes les options **Pandoc** : 

```
pandoc -d to_docx.yaml  

```

- Voir toutes les options dans le [Manuel Pandoc](https://pandoc.org/MANUAL.html#defaults-files)
- Exemples de fichiers *default* pour le [Mémoire de Landry](https://git.univ-pau.fr/jrabaud001/memoire-djeban-landry-koffi)
  - [`to_docx.yaml`](https://git.univ-pau.fr/jrabaud001/memoire-djeban-landry-koffi/-/blob/main/to_docx.yaml)
  - [`to_html.yaml`](https://git.univ-pau.fr/jrabaud001/memoire-djeban-landry-koffi/-/blob/main/to_html.yaml)
  - [`to_pdf_tex.yaml`](https://git.univ-pau.fr/jrabaud001/memoire-djeban-landry-koffi/-/blob/main/to_pdf_tex.yaml)

---

# Merci