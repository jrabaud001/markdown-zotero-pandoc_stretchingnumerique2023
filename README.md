[![](https://zenodo.org/badge/DOI/10.5281/zenodo.7756822.svg)](https://doi.org/10.5281/zenodo.7756822)

# Stretching numérique 2023 :<br>_Markdown_, _Zotero_, _Pandoc_ : écrire, citer, exporter

![logo stretching](supports/img/LogoStretchingNoir_0.png){width=40%} ![logo atelier](supports/img/logo-atelier.png){width=40%}

***

- Par ***Julien Rabaud*** | <julien.rabaud@univ-pau.fr>
  - twitter : [@UjuBib](https://twitter.com/UjuBib)
  - Mastodon : [@UjuBib@mamot.fr](https://mamot.fr/@UjuBib)
- Pour le [Stretching Numérique](https://www.stretchingnumerique.fr) 2023.
- Licence : ***cc-by***

***

## Dossier `supports`

- Fichier `stretching2023.md` : la source *markdown/css* de la présentation utilisée pendant l'atelier. Exportée en `pdf` et en `html` dans *VSCode* avec l'extension [vscode-reveal](https://github.com/tokiedokie/reveal-markdown).
- Fichier `stretching2023-MarkdownPandocZotero.pdf` : export `pdf` du précédent
- Sous-dossier `export` : ouvrir le fichier `index.html` pour voir la présentation dans le navigateur.

***

## Dossier `mindmap`

- Ouvrir le fichier `mindmap-pandoc.html` pour voir la collection de liens dans votre navigateur. Fichier généré dans *VSCode* à partir du fichier markdown du dossier par l'extension [markmap-vscode](https://github.com/markmap/markmap-vscode). Ajout manuel et minimal de code *html*, *css* et *javascript* pour l'affichage en overlay des liens qui l'autorisent.

***

## Dossier `Article`

- Fichier `readme.md` : commandes Pandoc pour exporter l'article d'Antoine Fauchié aux formats word, html et pdf
- Fichier `ArticleFauchie.md` : le texte au format markdown de l'article d'Antoine Fauchié : [Markdown comme condition d’une norme de l’écriture numérique](http://www.reel-virtuel.com/numeros/numero6/sentinelles/markdown-condition-ecriture-numerique), *Réel_Virtuel* n°6, 2017.
- Sous-dossier `img` : les figures de l'article.
- Sous-dossier `data` : 
  - `Article-Fauchie.bib` et `Article-Fauchie.json` : les références bibliographiques de l'article aux formats *BibTeX* et *csl-json* (exports *Zotero*). Un seul suffit, de préférence le `.json`.
  - `iso690-author-date-fr.csl` et `transversalites-Braud.csl` : 2 styles de citation (**C**itation **S**tyle **L**anguage), un *auteur-date* et un *notes de fin*. Pour que *Pandoc* formate ainsi les références bibliographiques.
  - `bleu.css` et `orange.css` : deux feuilles de styles css encapsulées dans une balise `<style>` destinées à être copiées par *Pandoc* dans l'en-tête des exports `html`. Elles placent différemment des éléments comme les notes ou la bibliographie.
  - `styleBraud.docx` : un fichier Word dont les styles ont été personalisés pour être utilisés lors des exports `.docx`
- Sous-dossier `sorties` : pour accueillir les exports *Pandoc* générés par les commandes proposées. Un seul fichier, pour exemple.

***

## Dossier `templates-pandoc`

- Une copie des **documents de référence** utilisés par défaut par *Pandoc* :
  - `.docx`
  - `.odt`  
  - `.pptx`

  **Ne modifier que les styles**.
- Une copie des **templates** utilisées par défaut par *Pandoc* : 
  - *html5*
  - *revealjs*
  - *latex*
  
  à modifier en utilisant le langage de templating de *Pandoc* :
  voir [Templates](https://pandoc.org/MANUAL.html#templates) dans le *Manuel Pandoc*.

***

## Dossier `mermaid`

Des exemples de diagrammes mermaid dans un fichier markdown.

```mermaid
graph LR

  subgraph écriture
  id1(Markdown)
  end

  subgraph références-Zotero
  id7>.bib, .json, .yaml...]
  end

  subgraph Conversion
  id2((Pandoc))
  end
	
  subgraph édition
  id3(.docx)
  id3b(.odt)
  id4(.tex)
  end

  subgraph Publication
  id5(.html)
  id6(.pdf)
  id9(.epub)
  end

  id8{{+ .css, ...}}
   
  id1-->id2
  id7-->id2
  id2-->id3 & id3b & id4 & id8
  id8 -->id5 & id9
  id4-->id6

%%lien vers le manuel Pandoc
  click id2 "pandoc.org/MANUAL.html" _blank

%%lien interne vers une note dans obsidian
  class id1 internal-link

  style écriture fill:lightgreen
  style Conversion fill:lightpink
  style édition fill:lightblue
  style Publication fill:gold
  style références-Zotero fill:burlywood

  style id2 color:blue,stroke:deeppink,stroke-width:6px
  style id1 stroke:seagreen,stroke-width:6px
  style id7 stroke:crimson,stroke-width:3px
  style id8 stroke:darkturquoise,stroke-width:3px
	
  classDef ed stroke:cornflowerblue,stroke-width:3px
  class id3,id3b,id4 ed

  classDef pub stroke:darkorange,stroke-width:3px
  class id5,id6,id9 pub
```

***
***

*Autres réalisations personnelles autour du format texte* : 

- [Atelier BU - Prendre des notes et rédiger en Markdown](https://atelier-markdown-uppa.netlify.app/#/)
- [SO-MATE 2022 Pau - Atelier *En mode texte*](https://markdown-somate2022.netlify.app/#/)
  - [Sources](https://git.univ-pau.fr/jrabaud001/so-mate_atelier_markdown_carnets)
- Accompagnement technique de l'écriture du mémoire de Master : [L’écriture de « l’analphabétisme » dans le récit autobiographique L’analphabète d’Agota Kristof](https://master2-kristof.netlify.app/)
  - [Sources](https://git.univ-pau.fr/jrabaud001/memoire-djeban-landry-koffi) 
- [Projet Blot (*documentation personnelle*)](https://inventaire-blot.netlify.app/#)
- [Mindmap autour d'Obsidian et Zotero](https://mindmap-obsidian.netlify.app)

***



